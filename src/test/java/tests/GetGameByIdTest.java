package tests;

import arrange.ReturnId;
import arrange.ReturnToken;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import pojo.GetCars;
import pojo.GetGameById;

import java.util.Arrays;
import java.util.List;

import static arrange.Specification.*;
import static io.restassured.RestAssured.given;

@Epic("Массив с играми")
@Tag("GET_test")
public class GetGameByIdTest extends ReturnToken {
    private static final String URL = "http://85.192.34.140:8080";
    private static final String subUrl_getAllGames = "/api/user/games";
    private static final String subUrl_getIdGme = "/api/user/games/";


    @Test
    @DisplayName("Получает все игры пользователя")
    @Description("Список с играми у пользователя")
    public void getCars() {
        installSpec(reqSpec(URL), respSpec_200());

        ReturnToken returnToken = new ReturnToken();

        List<GetCars> response = given()
                .auth().oauth2(returnToken.ttt())
                .when()
                .get(subUrl_getAllGames)
                .then()
                .log().all()
                .extract()
                .jsonPath().getList("", GetCars.class);

        //example1
//        List<GetCars> response2 = Arrays.asList(
//                given()
//                        .auth().oauth2(returnToken.ttt())
//                        .when()
//                        .get(subUrl_getAllGames)
//                        .then().extract().as(GetCars[].class));

        //example2
        int expId = 1401276124;
        response.forEach(x -> Assertions.assertEquals(expId, x.getGameId(),
                String.format("значение id %s не равно значению id %s", expId, x.getGameId())));

        Assertions.assertTrue(response.get(0).getDlcs().get(0).getSimilarDlc().getDlcNameFromAnotherGame().contains("string"));

        Assertions.assertTrue(response.get(0).getRequirements().getOsName().contains("string"));
    }

    @Test
    @DisplayName("Получает конкретную игру у пользователя по ID игры")
    public void getGameById() {
        installSpec(reqSpec(URL), respSpec_200());

        ReturnToken jwt = new ReturnToken();
        ReturnId id = new ReturnId();

        //example1
//        List<GetCarsById> response = given()
//                .auth().oauth2(jwt.ttt())
//                .when()
//                .get(subUrl_getIdGme + id.returnId())
//                .then().log().body()
//                .extract().jsonPath().getList("[0]", GetCarsById.class);
//
//        Assertions.assertEquals("string",response.get(0).getDlcs().get(0).getDlcName());

        //example2
        List<GetGameById> response = Arrays.asList(given()
                .auth().oauth2(jwt.ttt())
                .when()
                .get(subUrl_getIdGme + id.returnId())
                .then().log().all()
                .extract().as(GetGameById.class));

        Assertions.assertEquals("string", response.get(0).getDlcs());
    }

}
