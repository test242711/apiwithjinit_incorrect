package tests;

import arrange.ReturnToken;
import arrange.Specification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pojo.GetCarBrandsPOJO;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class GetCarBrands {
    private static final String URL = "http://85.192.34.140:8080";
    private static final String subUrl = "/api/easy/carBrands";

    @Test
    public void getBrands() {
        Specification.installSpec(Specification.reqSpec(URL), Specification.respSpec_200());

        ReturnToken returnToken = new ReturnToken();

        List<GetCarBrandsPOJO> response =
                given()
                        .when().get(subUrl)
                        .then()
                        .extract().jsonPath().getList("", GetCarBrandsPOJO.class);

        String expText = "Cordoba";
        String expRes = String.valueOf(response.stream().anyMatch(x->x.getModels().contains(expText)));

        Assertions.assertEquals("true",expRes,"Значения " + expText + " нет в списке getModels(), поэтому получаем false");
    }


}
