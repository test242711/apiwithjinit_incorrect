package arrange;

import pojo.GetToken;
import pojo.PostLoggAndPassToken;

import static arrange.Specification.*;
import static io.restassured.RestAssured.given;

public class ReturnToken {
    private static final String URL = "http://85.192.34.140:8080";
    private static final String subUrl = "/api/login";

    public String ttt(){
        installSpec(reqSpec(URL), respSpec_200());

        PostLoggAndPassToken postLoggAndPass = new PostLoggAndPassToken("string","string");

        GetToken getTokenFromPojo = given()
                .body(postLoggAndPass)
                .when()
                .post(subUrl)
                .then()
                .log().all()
                .extract().as(GetToken.class);

        String jwt = getTokenFromPojo.getToken();
        return jwt;
    }
}
