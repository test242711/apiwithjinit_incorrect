package arrange;

import pojo.GetCars;
import java.util.List;

import static arrange.Specification.*;
import static io.restassured.RestAssured.given;

public class ReturnId {
    private static final String URL = "http://85.192.34.140:8080";
    private static final String subUrl = "/api/user/games";

    public Integer returnId(){
        installSpec(reqSpec(URL), respSpec_200());

        ReturnToken returnToken = new ReturnToken();

        List<GetCars> response = given()
                .auth().oauth2(returnToken.ttt())
                .when()
                .get(subUrl)
                .then()
                .extract().jsonPath().getList("",GetCars.class);

        Integer id = response.get(0).getGameId();

        return id;
    }
}
