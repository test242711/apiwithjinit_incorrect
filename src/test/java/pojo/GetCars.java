package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetCars {
    private String description;
    private String dlcName;
    private Boolean isDlcFree;
    private Integer price;
    private Integer rating;
    private Integer hardDrive;
    private String osName;
    private Integer ramGb;
    private String videoCard;
    private String company;
    private Integer gameId;
    private String genre;
    private Boolean isFree;
    private Boolean requiredAge;
    private String title;
    private String dlcNameFromAnotherGame;
    private Boolean free;
    private String publishDate;
    private String publish_date;
    private SimilarDlc similarDlc;
    private Requirements requirements;
    private List<GetCars> dlcs;

    public GetCars(){
        super();
    }

    public List<GetCars> getDlcs() {
        return dlcs;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class SimilarDlc {
        private String dlcNameFromAnotherGame;
        private Boolean free;
    }

    public SimilarDlc getSimilarDlc() {
        return similarDlc;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Requirements{
        private String osName;
        private Integer ramGb;
        private Integer hardDrive;
        private String videoCard;
    }

    public Requirements getRequirements() {
        return requirements;
    }


}
