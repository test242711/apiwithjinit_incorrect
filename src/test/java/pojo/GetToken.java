package pojo;

public class GetToken {
    private String token;

    public GetToken(String token) {
        this.token = token;
    }

    public GetToken() {
        super();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
