package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import java.util.ArrayList;
import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public  class GetGameById {
    private Integer gameId;
    private String title;
    private String genre;
    private Boolean requiredAge;
    private Boolean isFree;
    private Double price;
    private String company;
    private Date publish_date;
    private Integer rating;
    private String description;
    private ArrayList<String> tags;
    private ArrayList<Dlc> dlcs;
    private Requirements requirements;

    public GetGameById() {
        super();
    }

    private static class Dlc {
        private Boolean isDlcFree;
        private String dlcName;
        private Integer rating;
        private String description;
        private Double price;
        private SimilarDlc similarDlc;
    }

    public static class Requirements {
        private String osName;
        private Integer ramGb;
        private Integer hardDrive;
        private String videoCard;
    }

    public static class SimilarDlc {
        private String dlcNameFromAnotherGame;
        private Boolean free;
    }

}
