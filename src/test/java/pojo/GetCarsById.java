package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetCarsById {
        private String description;
        private String dlcName;
        private Boolean isDlcFree;
        private Integer price;
        private Integer rating;
        private Integer hardDrive;
        private String osName;
        private Integer ramGb;
        private String videoCard;
        private String company;
        private Integer gameId;
        private String genre;
        private Boolean isFree;
        private Boolean requiredAge;
        private String title;
        private String dlcNameFromAnotherGame;
        private Boolean free;
}
