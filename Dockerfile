FROM openjdk:11

WORKDIR /app

COPY pom.xml /app/
COPY src /app/src

RUN apt-get update && \
    apt-get install -y maven && \
    mvn clean package

CMD ["mvn", "test"]